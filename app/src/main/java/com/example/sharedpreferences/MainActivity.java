package com.example.sharedpreferences;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private static  final String ARQUIVO_PREFERENCIA = "ArquivoPreferencia";
    private EditText editNome;
    private TextView textNome;

    @SuppressLint("MissingInflatedId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        editNome = findViewById(R.id.edit_nome);
        textNome = findViewById(R.id.text_nome);

        recuperarDados();

    }

        public void salvarDados(View view) {

        //recuperar o input do user
        String nome = editNome.getText().toString();

        if(!nome.isEmpty()) {
            SharedPreferences SharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);
            SharedPreferences.Editor editor = SharedPreferences.edit();
            editor.putString("meu_nome", nome);
            editor.apply();
        }else {
            editNome.setError("Informe seu nome");
        }
    }


        private void recuperarDados() {
        SharedPreferences SharedPreferences = getSharedPreferences(ARQUIVO_PREFERENCIA, 0);

        String nomeRecuperado = SharedPreferences.getString("meu_nome", "");
        textNome.setText(nomeRecuperado);


        }

}